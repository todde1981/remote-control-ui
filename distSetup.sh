#!/bin/sh
mkdir -p dist
rm -rf dist/*
find ./src/app \( -name "*.html" -o -name "*.css" \) -type f -exec cp {} dist \;
cp -r ./src/css ./src/fonts ./src/*.html ./src/*.ico  dist
cp node_modules/reflect-metadata/Reflect.js node_modules/zone.js/dist/zone.js dist/
sed -i '' /'<!-- remove for bundle usage begin -->'/,/'<!-- remove for bundle usage end -->'/d dist/index.html
sed -i '' s/'<!-- prod scripts begin'/'<!-- remove for bundle usage end -->'/ dist/index.html
sed -i '' s/'prod scripts end -->'/'<!-- prod scripts end -->'/ dist/index.html
