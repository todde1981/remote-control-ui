#!/bin/sh
mkdir -p build
rm -rf build/*
cp -rf src/* build
find ./build -name "*.ts" -type f -exec rm {} \;
cp -f src/systemjs.config.js build/systemjs-dist.config.js
sed -i '' s/\'app\',/\''build\/app'\',/ build/systemjs-dist.config.js
