import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES, Routes, Router } from '@angular/router';

import {RemoteControlUiApp} from '../remote-control-ui/remote-control-ui.component';
import { APP_ROUTER_PROVIDERS } from './app-router.providers';

@Component({
    moduleId: module.id,
    selector: 'app-container',
    template: `<router-outlet></router-outlet>`,
    directives: [ROUTER_DIRECTIVES],
    providers: [APP_ROUTER_PROVIDERS]
})
@Routes([
    { path: '/', component: RemoteControlUiApp },
    { path: '*', component: RemoteControlUiApp }
])
export class AppRouterComponent {

    constructor(private router: Router) {

    }

}
