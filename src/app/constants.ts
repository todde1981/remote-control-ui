import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
    public Server: string = "http://192.168.72.20:8002/";
    public ServerWithReceiverUrl = this.Server + 'receivers';
    public ServerWithTvUrl = this.Server + 'tvs';
}
