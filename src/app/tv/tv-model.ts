export class TvModel {
    public key: String;
    public description: String;
    public id: Number;
    public host: String;
    public timeout: Number;
    public port: Number;
    public name: String;
}
