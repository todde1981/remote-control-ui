import {bootstrap} from '@angular/platform-browser-dynamic';
import {enableProdMode} from '@angular/core';
import {environment} from './environment/environment';
import { AppRouterComponent } from './router/app-router.component';
import { Configuration } from './constants';

if (environment.production) {
    enableProdMode();
}

bootstrap(AppRouterComponent, [Configuration]).then(
    success => '',
    error => console.log(error)
);
