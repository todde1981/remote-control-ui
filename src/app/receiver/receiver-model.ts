export class ReceiverModel {
    public volume: number;
    public on: Boolean;
    public mute: Boolean;
    public inputs: Object;
    public id: Number;
    public input: String;
}
