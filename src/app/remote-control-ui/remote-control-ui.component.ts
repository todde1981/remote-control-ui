import {Component, OnInit} from '@angular/core';
import { ROUTER_DIRECTIVES} from '@angular/router';
import { DROPDOWN_DIRECTIVES} from 'ng2-bootstrap';
import { ReceiverService } from '../receiver/receiver-service';
import { ReceiverModel } from '../receiver/receiver-model';
import { TvService } from '../tv/tv-service';
import { TvModel } from '../tv/tv-model';
import { VolumeModel } from './volume-model';
import { InputModel } from './input-model';

@Component({
    moduleId: module.id,
    selector: 'remote-control-ui-app',
    templateUrl: 'remote-control-ui.component.html',
    styleUrls: ['remote-control-ui.component.css'],
    providers: [ReceiverService, TvService],
    directives: [ROUTER_DIRECTIVES, DROPDOWN_DIRECTIVES],
    pipes: []
})
export class RemoteControlUiApp implements OnInit {
    public volIntervals: VolumeModel[] = [
        { name: "Volume: 1", value: 1 },
        { name: "Volume: 5", value: 5 },
        { name: "Volume: 10", value: 10 }
    ];
    public selectedVolInterval: VolumeModel = this.volIntervals[1];
    public inputs: InputModel[] = [
        { name: "Fire Tv", receiverInput: "HDMI1", controls: null, tvInput: "HDMI1" },
        { name: "PS4", receiverInput: "HDMI2", controls: null, tvInput: "HDMI1" },
        { name: "PS3", receiverInput: "HDMI4", controls: null, tvInput: "HDMI1" },
        { name: "WII U", receiverInput: "HDMI3", controls: null, tvInput: "HDMI1" },
        { name: "XBOX", receiverInput: "HDMI5", controls: null, tvInput: "HDMI1" },
        { name: "Switch", receiverInput: "V-AUX", controls: null, tvInput: "HDMI1" },
        { name: "Tv", receiverInput: "AV1", controls: null, tvInput: "InputTV" },
        { name: "RetroPi", receiverInput: "AV1", controls: null, tvInput: "HDMI2" },
        { name: "Tuner", receiverInput: "TUNER", controls: "http://192.168.72.2", tvInput: "HDMI1" },
        { name: "Net Radio", receiverInput: "NET RADIO", controls: "http://192.168.72.2", tvInput: "HDMI1" }
    ];
    public myInput: InputModel = this.inputs[0];
    public myTv: TvModel;
    public myReceiver: ReceiverModel;
    public muted: Boolean = false;

    constructor(private _tvService: TvService, private _receiverService: ReceiverService) { }

    ngOnInit() {
        this.getTv();
        this.getReceiver();
    }

    private getTv(): void {
        this._tvService
            .GetSingle(1)
            .subscribe((data: any) => this.myTv = data,
            error => console.log(error),
            () => '');
    }

    private getReceiver(): void {
        this._receiverService
            .GetSingle(1)
            .subscribe((data: any) => this.myReceiver = data,
            error => console.log(error),
            () => this.muted = this.myReceiver.mute);
    }

    private sendTvCommand(key: String, nextCommand: any = null): void {
        this.myTv.key = key;
        this._tvService.Update(1, this.myTv)
            .subscribe((data: any) => '',
            error => console.log(error),
            () => nextCommand);
    }

    private sendReceiverCommand(): void {
        this._receiverService.Update(1, this.myReceiver)
            .subscribe((data: any) => '',
            error => console.log(error),
            () => '');
    }

    turnOn(input: any, $event: any) {
        this.sendTvCommand("PowerOn", this.sendTvCommand("HDMI1"));
        this.myReceiver.on = true;
        this.myReceiver.input = "HDMI1";
        this.myReceiver.volume = -30;
        this.sendReceiverCommand();
        this.myInput = this.inputs[0];
    }

    turnOff(input: any, $event: any) {
        this.sendTvCommand("PowerOff");
        this.myReceiver.on = false;
        this.sendReceiverCommand();
    }

    changeInput(input: InputModel, $event: any) {
        this.sendTvCommand(input.tvInput);
        this.myReceiver.input = input.receiverInput;
        this.sendReceiverCommand();
        this.myInput = input;
    }

    upVolume(input: any, $event: any) {
        this.myReceiver.volume += this.selectedVolInterval.value;
        this.sendReceiverCommand();
    }

    downVolume(input: any, $event: any) {
        this.myReceiver.volume -= this.selectedVolInterval.value;
        this.sendReceiverCommand();
    }

    mute(input: any, $event: any) {
        this.myReceiver.mute = !this.myReceiver.mute;
        this.muted = this.myReceiver.mute;
        this.sendReceiverCommand();
    }

    upChannel(input: any, $event: any) {
        this.sendTvCommand("ChannelUp");
    }

    downChannel(input: any, $event: any) {
        this.sendTvCommand("ChannelDown");
    }

    isNotTv() {
        return this.myInput.name != "Tv";
    }
}
