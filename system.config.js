//configure system loader
System.config({
	map: {
		'rxjs': 'node_modules/rxjs',
		'@angular': 'node_modules/@angular',
		'ng2-bootstrap': 'node_modules/ng2-bootstrap/',
		'moment': 'node_modules/moment',
		'app': 'dist'
	},
	packages: {
		'app': {
			main: 'main.js',
			defaultExtension: 'js'
		},
		'@angular/core': {
			main: 'index.js',
			defaultExtension: 'js'
		},
		'@angular/compiler': {
			main: 'index.js',
			defaultExtension: 'js'
		},
		'@angular/common': {
			main: 'index.js',
			defaultExtension: 'js'
		},
		'@angular/platform-browser': {
			main: 'index.js',
			defaultExtension: 'js'
		},
		'@angular/platform-browser-dynamic': {
			main: 'index.js',
			defaultExtension: 'js'
		},
		'rxjs': {
			defaultExtension: 'js'
		},
		'ng2-bootstrap':{
			main: 'ng2-bootstrap.js',
			defaultExtension: 'js'
		},
		'moment':{
			main: 'moment.js',
			defaultExtension: 'js'
		}
	}
});
